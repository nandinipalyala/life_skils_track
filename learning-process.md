# How to learn Faster with the Feynman Technique

## **1) What is Feynman Technique?** 
Feynman technique is a technique, that deepens the understanding of anything whether we know it or not in a much more efficient and simpler way. Feynman is a scientist who introduced the Feynman technique. He got a Noble prize in physics for working on Quantum electrodynamics in the year of 1965. Feynman is a great teacher and he is well known as "The Great Explainer".


Explaining a concept helps to improve our understanding of that concept in basically any area. Feynman's first principle is that you must not fool yourself and you are the easiest person to fool. One can understand any topic at a deeper level with the help of the Feynman technique. It saves time and gives good results.

**There are four steps to follow the Feynman technique. They are :**

1. Take a piece of paper and write the concept's name at the top.
2. Explain the concept using simple language.
3. Identify problem areas, then go back to the sources to review.
4. Pinpoint any complicated terms and challenge yourself to simplify them.

Simply, if we want to understand anything well, we have to try to explain it. If we can't explain it well, it means we don't understand it well enough.

## **2) Different ways to implement the Feynman technique in my learning process :**
* Select a topic and try to research it and learn.
* Explain the topic to myself by seeing in the mirror which helps to boost my confidence.
* Identify any difficulty while explaining a topic and then revise the resource material again to better understand it.
* Try to compare the things in a particular topic if I am not able to explain a topic.
* If I am sure about a particular topic, I try to explain it to another person who is very well aware of that topic.




# Learning how to learn :

## **3) Paraphrasing the video in my own words**

**There are two modes to study. They are :**

1. Focus mode
2. Diffuse mode

### **Focus mode :**
Consistent attention on something is called Focus mode. 
We think deeply about something when we focus on learning.
If we already know about a topic or not perfect at it, then we can learn efficiently by focusing on it within a short time. 
A focus mode helps to learn things faster and in an efficient way without any distractions.

### **Diffuse mode :**
Thinking about a topic in a relaxed state is called Diffuse mode. So, diffuse mode is considered as a resting state. Diffuse mode helps to understand the topic slowly when we are stuck at some points.

### **Pinball Machine Analogy :**

If we are focused on something, trying to learn a new concept or solve a problem and we get stuck, we want to turn our attention away from that problem and allow the diffuse modes. These resting states do their work in the background.
A different way of thinking is required if we get stuck or know something about the topic.

### **Procrastination :**
Procrastination should be avoided if we want to focus on something.when we feel unhappy, we move our attention to a more pleasant task and then feel happy. But this kind of habit should be avoided.

### **Best practices to focus on learning :**

* It's better to use the Pomodoro technique. It is to focus and study any concept for 25 minutes and take a break for 5 to 10 minutes and then continue with the learning again.
* Slowly learning the topics is also fine if we are consistent towards the work.
* Have to test ourselves all time on any topic by doing exercises on that topic efficiently.
* Understanding clearly is more important.


## **4) Some of the steps that I can take to improve my learning process :**

* Create a suitable environment to study without any distractions.
* Set some targets and understand clearly what to study in detail.
* Follow the Pomodoro technique to study and learn effectively and create a proper schedule.
* I try to test my learnings all time whenever I finish learning a concept. 
* I try to practice until and unless I get full clarity on a particular topic.
* Whenever I stuck I try to listen to music and think about the problem or topic in a relaxed mode until I get an idea about it.
* I share my schedule with my family and friends so that I don't get disturbed by them.
* I block all the distractions like using social media and mobile.
* I keep a record of all the tasks that I have completed.

# Learn Anything in 20 hours

## **5) Key takeaways from the video**

It takes 10,000 hours to become good at something in the point of view Gladwell. But it is not required that much time to learn something. It takes 20 hours of focused deliberate practice to learn something new. The learning theory proposes that a learner's efficiency in a task improves over time the more the learner performs the task. Practice things based on how good we are and how much time we need to spend to learn efficiently. We have to learn anything efficiently.

### **Different ways to follow 20 hours rule :**

#### **1) Deconstruct the skill :**

We have to decide exactly what we want to be able to do when we are done and then look into the skill and break it down into smaller pieces.
Most of the things that we think of as skills are actually big bundles of skills that require all sorts of different things.
The more we can break apart the skill, the more we are able to decide, what parts of this skill would actually help us get to what we want and then we can practice those first.
If we practice more important things at first, we will be able to improve our performance in the least amount of time.

#### **2) Learn enough to self-correct :**
Practise whatever we learn without any procrastination.
We need to get three to five resources about the topic which we want to learn. Resources like books, DVDs, and courses.

#### **3) Remove practice barriers :**
Removing practice barriers like procrastination. It helps in a better understanding of a concept.

#### **4)Practice at least 20 hours :**
Consistent practice for 20 hours always leads to a better result. We can learn it easily and in an effective way.

## **6) Some of the steps to follow while approaching a new topic :**

*  **Setting a goal :** Setting a goal and knowing its result is the most crucial step in learning anything.
* **Break the learning into parts:** One cannot learn a huge concept at a time. Learning the concept by breaking it into sub-concepts will helps to remember and learn things in a proper way.
* **Focus :** Constant attention to learn something.
* **Teach :** Teaching whatever we learn helps to understand the topic in a much better way.
* **Test :** Test whatever we learn by doing exercises.
* **Remove barriers :** By removing barriers like procrastination we can learn the concepts easily.
* **Practising 20 hours :** Consistency effort of practising

