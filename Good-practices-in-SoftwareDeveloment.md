# Good Practices for Software Development

## Review Questions

# Question 1
What is your one major takeaway from each one of the 6 sections. So 6 points in total.
### Answer
## 1. Gathering requirements
* Make notes while discussing requirements with your team.
## 2. Always over-communicate: Some scenarios
* Use the group chat/channels to communicate most of the time. This is preferable over private DMs simply because there are more eyes on the main channel.
## 3. Stuck? Ask questions
* Explain the problem clearly, mention the solutions you tried out to fix the problem
## 4. Get to know your teammates
* Join the meetings 5-10 mins early to get some time with your team members
## 5. Be aware and mindful of other team members
* Be available when someone replies to your message. Things move faster if it's a real-time conversation as compared to a back and forth every hour.
## 6. Doing things with 100% involvement
* Always keep your phone on silent. Remove notifications from the home screen. Only keep notifications for work-related apps.
# Question 2
### Which area do you think you need to improve on? 
### Answer
* I need improvement on the area called **Stuck? Ask questions**.


### What are your ideas to make progress in that area?
### Answer
* Making an effort to understand the problem and researching before asking question.
* Continuously seeking feedback from seniors.
* Rather than spending more time to find the solution, I will try to seek help from the teammates.