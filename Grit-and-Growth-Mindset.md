# Grit and Growth Mindset
## **1. Grit**
### Question 1
### Paraphrase (summarize) the video in a few lines. Use your own words.

1. Grit is passion and preservance for very long-term goals.
2. Grit is having stamina
3. Grit is sticking with our future.
4. Not just for months, but for years and working really hard to make that future a reality.
5. It is found in research that grit is the driving force for any individual toward learning.
6. To learn anything we need the courage to be on that path which is not easy to pursue, and for that courage, we need to be dedicated toward a growth mindset.
7. Irrespective of individual skill set, IQ, and other factors, if anyone wants to learn then they just needs to be keen to learn and should have strong willpower.

### Question 2
### What are your key takeaways from the video to take action on?
1. Aim to improve yourself Every Single Day
2. The way to grow our grit is to remind ourselves of the greater purpose. 
3. Develop a Fascination with what we 
are trying to do.Higher levels of purpose directly correlate to higher levels of grit
4. Adopt a growth Mindset.
5.  We can develop the confidence to start taking action despite how untalented we think we are.

## **2. Introduction to Growth Mindset**
### Question 3
### Paraphrase (summarize) the video in a few lines in your own words.
1. People have two types of mindsets
2. These mindsets are really important
when it comes to learning.
3. Some people have fixed mindset and some others have Growth Mindset.
4. People with fixed mind believe that skills and intelligence are set.
5. People with a fixed mindset believe that you can't or don't have to learn and grow.
6. They believe that they are not in control of their abilities.
7. People with Growth Mindset believe that skills and intelligence are grown and developed.
8. So people who are good at something are good because they built that ability,and people who aren't are not good because they haven't done the work.
9. They believe that they are in control
of their abilities.
10. People with a growth mindset believe that skills are built.
11. People with a growth mindset do believe in their capacity to learn and grow.

### Question 4
### What are your key takeaways from the video to take action on?
1. The growth mindset is the belief that we can grow our brain and that our intelligence grows with effort.
2. The growth mindset allows people to value what they’re doing regardless of the outcome.
3. A fixed mindset is the belief that we are born a certain way and cannot change.
4. In the fixed mindset, everything is about the outcome. If you fail—or if you’re not the best—it’s all been wasted.

## **3. Understanding Internal Locus of Control**
### Question 5
### What is the Internal Locus of Control? What is the key point in the video?
* Internal Locus of Control: They believed that it was factors they controlled that led to their outcomes it was their hard work and their extra
effort that allowed them to do well on the puzzles right because how much work we put into something is something that we have complete control.Staying motivated we must feel like we have control over our life and that we are responsible for the things that happen to us if we want to feel motivated all of the time. Extremely motivated individual to be able to face hundreds and hundreds of rejections every single day.

* This video emphasised a number of important ideas to keep in mind, including the need to maintain a positive outlook, stay motivated, and look for justifications rather than justifications when performing tasks.
Be a doer, not an excuse maker, and realise that our biggest obstacle to success isn't physical but rather emotional, according to the lesson this film taught us. 

## **4. How to build a Growth Mindset**
### Question 6
### Paraphrase (summarize) the video in a few lines in your own words.
* A growth mindset begins with the
fundamental number one rule in life and
that is believe in our ability to figure things out.
* If we have a goal but are unsure about how to accomplish it, we must think in a way that will help us find a solution.
* Second, we must challenge our presumptions and pessimistic beliefs.
* Thirdly, we need to build an openness to new ideas into our life curriculum.
* Finally, respect the battle since it will make us stronger and enable us to maintain our composure in the face of turmoil. 

### Question 7
### What are your key takeaways from the video to take action on?
1. Take time to acknowledge, reflect, and embrace all our failures
2. Part of developing a growth mindset is shattering the negative perception of a challenge. 
3. Embrace challenges and view them as fruitful learning experiences that we would not get otherwise. 
4. Create clear, realistic goals based on our passion and purpose. 
5. And be sure to give ourself enough time to conquer them thoroughly.  

##   **5. Mindset - A MountBlue Warrior Reference Manual**
### Question 8
### What are one or more points that you want to take action on from the manual?
1. I will stay with a problem till I complete it. I will not quit the problem.
2. I will understand each concept properly.
3. I will take ownership of the projects assigned to me. Its execution, delivery and functionality is my sole responsibility.
4. I will always be enthusiastic. I will have a smile on my face when I face new challenges or meet people.
