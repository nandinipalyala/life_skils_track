# Energy Management

### 1.What are the activities you do that make you relax-Calm quadrant?

* Listening to music.
* Spending time with friends and Family.
* Going for a walk.

### 2.When do you find getting into the Stress quadrant?

* While doing things that I don't like and doing them for long hours.
* while doing the project even though I didn't understand its concept.

### 3.How do you understand if you are in the Excitement quadrant?

* While my feet start sweating.
* While I am afraid of something.

### 4.Paraphrase the Sleep is your Superpower video in detail

* Sleeping for seven hours or more will boost testosterone levels.
* Statistics say that people who sleep more can learn 40% more than people who sleep less.
* We can concentrate more if we sleep properly.

### 5.What are some ideas that you can implement to sleep better?
* Follow a regular schedule to sleep.
* Maintain a good environment to sleep in.

### 6.Paraphrase the video-Brain Changing Benefits of Excercise.Minimum 5 points.
* Excercise improves our focus and concentration.
* It promotes the growth of new brain cells.
* Boosts our mood and memory.
* Excercise decreases feeling of anxiety.
* It protects our brain from aging and neurodegenerative diseases.

### 7.What are some steps you can take to exercise more?
* I walk daily.
* Regularly doing some exercise workouts.