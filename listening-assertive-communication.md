# Listening and Active Communication

## 1) What are the steps/strategies to do Active Listening?

**Active Listening :**
The act of fully hearing and comprehending the meaning of what someone else is saying.

* Avoid getting distracted by your own thoughts.
* Focus on the speaker and topic.
* Try not to interrupt the other person.
* Let them finish and then respond.
* Use door openers like Tell me more,Go ahead,That sounds interesting to show that you are interested and keep the other person talking.
* Show that you are listening with your body language.
* Take notes during important conversations.

## 2) According to Fisher's model, what are the key points of Reflective Listening?

* Hearing and understanding what the other person is communicating through words and
body language to the best of your ability.
* Responding to the other person by reflecting the thoughts and feelings you heard in his or
her words, tone of voice, body posture, and gestures.
* Actively engaging in the conversation.
* Genuinely listening from the speaker's point of view.

## 3) What are the obstacles in your listening process?

* External noise like the sound of equipment running, phones ringing, or other people having conversations.
* Visual distractions like the scene outside a window or the goings-on just beyond the glass walls of a nearby office.
* I may become distracted by the other person’s personal appearance, mannerisms, voice, or gestures.
* Getting distracted while the other person is speaking.
* Sometimes anxiety leads to an obstacle for me.

## 4) What can you do to improve your listening?

* Avoid getting distracted by my own thoughts.
* Try not to interrupt the other person.
* I consider eye contact. I turn my face to the conversation member out of politeness.
* I will be alert throughout the conversation.
* I pay attention to nonverbal signs such as body language and tone.
* I allow myself to create a mental image of the information you are hearing.
* Keep my workspace neat and turn off.

## 5)When do you switch to Passive communication style in your day to day life?

* I will switch into passive communication When I am in a meeting for the first time.
* I will switch to passive communication,if I am not sure of what's going on in a conversation among people.
* I switch to passive communication if I feels anxiety.

## 6) When do you switch into Aggressive communication styles in your day to day life?


* I switch into Aggressive communication if I am tired and not have enough patience to do the work.
* I will switch into aggressive communication,if someone interrupts me, while I am talking sometimes.


## 7) When do you switch into Passive Aggressive (sarcasm/gossiping/taunts/silent treatment and others) communication styles in your day to day life?

* I will switch into Passive Aggressive When I am bored by the person speaking with me.
* I will switch into passive aggressive when I feel insecure.
* If I am lacking self-esteem,I will switch into Passive Agrressivee.

## 8) How can you make your communication assertive? You can analyse the videos and then think what steps you can apply in your own life?

* I stand up for myself.
* I say no without any guilty.
* Maintain self control.
* I rehearse what I want to say.
* I Pay attention to the speaker.
* Genuinely listening from the speaker's point of view.
* Avoid interrupting the speaker.