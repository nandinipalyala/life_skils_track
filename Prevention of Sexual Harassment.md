# Types of behaviour cause Sexual Harassment

## Sexual Harassment

Any unwelcome verbal,visual, or physical conduct of a sexual nature that is severe or pervasive and affects working conditions or creates a hostile work environment is said to be Sexual Harassment

### Three forms of Sexual Harassment :

1. Verbal Harassment
2. Visual Harassment
3. Physical Harassment

1)  **Verbal Harassment**: 

* Comments about clothing, a person's body, sexual or gender based jokes or remarks.
* Requesting sexual favours or repeatedly asking a person out.
* It also includes sexual innuendos,threats,spreading rumors about a person's personal or sexual life.
* Usage of foul and obscene language.

2) **Visual Harassment**:

Looking at someone without their consent, touching them without permission, making lewd gestures or  displaying sexually suggestive pictures/videos.

3) **Physical Harassment**:

Touching someone without their consent, pushing, shoving, spitting on them, etc.

### Two categories of Sexual Harassment

1. Quid Pro Quo
2. Hostile Work Environment

1) Quid Pro Quo:

+ It means this for that. This happens when an employer or supervisor uses job rewards or punishments to coerce an employee into a sexual relationship or sexual act. 
+ This includes offering the employee raises or promotion or threatening demotions firing or other penalties for not complying with the request a hostile work environment occurs when employee behaviour interfaces with the work performance of another or creates an intimidating or offensive work place.

2) Hostile Work Environment

+ Hostile work environment sexual harassment is any conduct directed at an employee because of that employee’s sex that unreasonably interferes with the employee’s work performance or creates an intimidating, hostile or offensive working environment. 
+ This usually happens when someone makes repeated sexual comments and makes another employee feel so uncomfortable that their work performance suffers.

# I will do the following things If I face or witness sexual harassment

* I try to say harasser it is not a correct way.
* I try to distract the harasser by interrupting them. I try to deviate the topic.
* If the harasser won't listen, I ask for someone else to help.
* I will make a document on the incident that happened.
* I will note down date,time and location of the harassment.
* I keep all the records related to the harassment
* I report the harassment issue to the assigned authorities.
* I support and help people if
* I witness harassment.


## Conclusion

* Summarize the key findings and recommendations presented in the paper.
* Emphasize the importance of collective efforts in preventing sexual harassment and creating safe and respectful environments for all.

